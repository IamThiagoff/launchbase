 <h1 align="center">
    <img alt="Launchbase" src="https://storage.googleapis.com/golden-wind/bootcamp-launchbase/logo.png" width="300px" />
</h1>

<h3 align="center">
  Challenge 2: Rocketseat Website
</h3>
 <p align="center"> applications developed during Rocketseat's Launchbase bootcamp </P>  
<h2> <img src= "https://img.icons8.com/plasticine/2x/rocket.png" width="50px" height="50px" align="center"/> What was created? </h2>
<ol> <li> First HMLT: simple "home" page using HTML and CSS; </li>
<li> Description page: simple "about us" page using HTML and CSS; </li>
<li> Courses and iframe page: couses page using JavaScript to create a pop-up with information from the oficial Rocketseat website. </li> </ol> </p>

<h2> <img src="https://i.dlpng.com/static/png/6577858_preview.png" width="50px" align="center"/> How to use </h2>
 <ul style= circle> <li> You need to install  <a href="https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer">Live Server</a> on VSCode </li> 
 <li> Make a copy of this repository </li> 
